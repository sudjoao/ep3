# ep3

Exercício Programa 3 OO FGA 2018/2  
 Nós decidimos criar uma aplicação que consiste em um blog de auto-ajuda,chamado All for All.  
Álex Porto Ferreira 170119815
João Pedro José Guedes 170013910
## Como usar o projeto
 * As gems que utilizamos foi a Devise pra fazer o cadastro e a RailsAdmin para gerar o administrador da pagina e a RailsRoady para gerar o diagrama de classes.
 * No blog,é possível criar um usuário e também efetuar login caso ja tenha cadastro. Após logado,usuário vai para a página principal e pode escolher,na barra de menu superior,o que deseja fazer:ver os posts já criados ou querer saber mais sobre o site.
 * Clicando no botão de posts,verá todos já criados e clicando em determinado post ele pode fazer comentários.  
 * Caso queira criar um post,basta clicar no botão novo post e preencher o campo de texto e é possível editar e deletar o post.Tanto o comentário tanto quanto o post são feitos anonimamente,não sendo necessário identificação.
 * Na aba 'Sobre' estamos falando mais sobre o nosso site e também disponibilizando telefones de psicólogos gratuitos de faculdades/universidades em Brasília.  
 * E o administrador pode fazer basicamente tudo,como editar/apagar/criar posts,editar/apagar/deletar comentarios e tambem sabe quem foi que fez todos os posts e comentários além de também poder criar novos usuários.
 * Grande parte do front foi feito utilizando o Bootstrap, que é um framework para auxiliar na criação de html, css etc.

