Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'home/index'
  root "home#index"
  devise_for :users
  resources :posts do
    resources :comments
  end

  root "posts#index"

  resources :posts
  root "posts#index"

  get 'about' => 'home#about', as: 'about'
end

